/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_TAG_LOCATIONS_H
#define VOXL_TAG_LOCATIONS_H

#include <stdio.h>
#include <string.h>
#include <modal_json.h>
#include <modal_pipe_interfaces.h>


#ifdef __cplusplus
extern "C" {
#endif

/**
 * This is the default and recommended config file path and is used by both
 * voxl-vision-px4 and voxl-apriltag-server. However other projects can elect
 * to have their own independant config files if they wish.
 */
#define TAG_LOCATION_PATH	"/etc/modalai/tag_locations.conf"

/*
 * recommended sane but arbitrary limit on number of tags in one config file to
 * allow for static memory allocation. You can ignore this and set your own
 * limit if you want, both functions here allow setting max through arguments.
 */
#define MAX_TAG_LOCATIONS	64


// currently only one tag type supported
#define TAG_TYPE_APRILTAG_36H11		0

// struct to contain all data from each single tag entry in config file
typedef struct tag_location_t{
	int id;							///< ID of the tag
	char name[TAG_NAME_LEN];		///< optional tag name, e.g. "landing pad"
	int loc_type;					///< location type: fixed, static, or dynamic
	int tag_type;					///< currently ignored, only apriltag 36h11 supported for now
	float size_m;					///< tag size in meters, length of one edge
	double T_tag_wrt_fixed[3];		///< translation vector
	double R_tag_to_fixed[3][3];	///< rotation matrix
	int reserved;					///< reserved for future use
} tag_location_t;



/**
 * @brief      print out an array of apriltag configuration structs
 *
 * @param      t     pointer to conf struct array
 * @param[in]  n     number of tags to print
 */
void tag_location_print(tag_location_t* t, int n, float default_size_m);


/**
 * @brief      Reads an apriltag configuration file.
 *
 * @param[in]  path            file path, usually APRILTAG_CONFIG_PATH
 * @param      t               pointer to array of config structs to write out
 *                             to
 * @param      n               pointer to write out number of tags parsed from
 *                             the file
 * @param[in]  max             The maximum number of tags that array t can hold
 * @param      default_size_m  pointer to float in which to write the size that
 *                             unlisted tags will assume to have.
 *
 * @return     0 on success, -1 on failure
 */
int tag_location_read_file(const char* path, tag_location_t* t, int* n, int max, float* default_size_m);



#ifdef __cplusplus
}
#endif

#endif // end #define VOXL_TAG_LOCATIONS_H