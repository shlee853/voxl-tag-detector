/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>

#define CONF_FILE "/etc/modalai/voxl-tag-detector.conf"
#define MAX_CH	3

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration parameters for voxl-tag-detector.\n\
 * You can specify up to 3 cameras to do detection on simultaneously.\n\
 * For the stereo camera pair, only the left camera is used.\n\
 */\n"


typedef struct detector_config_t{
	int enable;
	char input_pipe[MODAL_PIPE_MAX_DIR_LEN];
	int en_fast_mode;
	int n_threads;
	int en_undistortion;
	float undistort_scale;
	char overlay_name[MODAL_PIPE_MAX_NAME_LEN];
	char lens_cal_file[MODAL_PIPE_MAX_PATH_LEN];
	int skip_n_frames;
} detector_config_t;

static detector_config_t conf[MAX_CH];


/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
	for(int i=0; i<MAX_CH; i++){
		printf("detector #%d\n", i);
		printf("    enable:          %d\n", conf[i].enable);
		printf("    input_pipe:      %s\n", conf[i].input_pipe);
		printf("    en_fast_mode:    %d\n", conf[i].en_fast_mode);
		printf("    n_threads:       %d\n", conf[i].n_threads);
		printf("    en_undistortion: %d\n", conf[i].en_undistortion);
		printf("    undistort_scale: %4.2f\n", (double)conf[i].undistort_scale);
		printf("    overlay_name:    %s\n", conf[i].overlay_name);
		printf("    lens_cal_file:   %s\n", conf[i].lens_cal_file);
		printf("    skip_n_frames:   %d\n", conf[i].skip_n_frames);
	}
	printf("=================================================================\n");
	return;
}


/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;

	cJSON* child = json_fetch_object_and_add_if_missing(parent, "detector_0");
	json_fetch_bool_with_default(	child, "enable",		&conf[0].enable,		1);
	json_fetch_string_with_default(	child, "input_pipe",	conf[0].input_pipe,		MODAL_PIPE_MAX_DIR_LEN, "tracking");
	json_fetch_bool_with_default(	child, "en_fast_mode",	&conf[0].en_fast_mode,	1);
	json_fetch_int_with_default(	child, "n_threads",		&conf[0].n_threads,	1);
	json_fetch_bool_with_default(	child, "en_undistortion",&conf[0].en_undistortion,	1);
	json_fetch_float_with_default(	child, "undistort_scale",&conf[0].undistort_scale,	0.6);
	json_fetch_string_with_default(	child, "overlay_name",	conf[0].overlay_name,	MODAL_PIPE_MAX_DIR_LEN, "tracking_tag_overlay");
	json_fetch_string_with_default(	child, "lens_cal_file",	conf[0].lens_cal_file,	MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_tracking_intrinsics.yml");
	json_fetch_int_with_default(	child, "skip_n_frames",	&conf[0].skip_n_frames,	5);

	child = json_fetch_object_and_add_if_missing(parent, "detector_1");
	json_fetch_bool_with_default(	child, "enable",		&conf[1].enable,		0);
	json_fetch_string_with_default(	child, "input_pipe",	conf[1].input_pipe,		MODAL_PIPE_MAX_DIR_LEN, "stereo");
	json_fetch_bool_with_default(	child, "en_fast_mode",	&conf[1].en_fast_mode,	1);
	json_fetch_int_with_default(	child, "n_threads",		&conf[1].n_threads,	1);
	json_fetch_bool_with_default(	child, "en_undistortion",&conf[1].en_undistortion,	1);
	json_fetch_float_with_default(	child, "undistort_scale",&conf[1].undistort_scale,	0.9);
	json_fetch_string_with_default(	child, "overlay_name",	conf[1].overlay_name,	MODAL_PIPE_MAX_DIR_LEN, "stereo_tag_overlay");
	json_fetch_string_with_default(	child, "lens_cal_file",	conf[1].lens_cal_file,	MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_intrinsics.yml");
	json_fetch_int_with_default(	child, "skip_n_frames",	&conf[1].skip_n_frames,	5);

	child = json_fetch_object_and_add_if_missing(parent, "detector_2");
	json_fetch_bool_with_default(	child, "enable",		&conf[2].enable,		0);
	json_fetch_string_with_default(	child, "input_pipe",	conf[2].input_pipe,		MODAL_PIPE_MAX_DIR_LEN, "extra");
	json_fetch_bool_with_default(	child, "en_fast_mode",	&conf[2].en_fast_mode,	1);
	json_fetch_int_with_default(	child, "n_threads",		&conf[2].n_threads,	1);
	json_fetch_bool_with_default(	child, "en_undistortion",&conf[2].en_undistortion,	0);
	json_fetch_float_with_default(	child, "undistort_scale",&conf[2].undistort_scale,	1.0);
	json_fetch_string_with_default(	child, "overlay_name",	conf[2].overlay_name,	MODAL_PIPE_MAX_DIR_LEN, "extra_tag_overlay");
	json_fetch_string_with_default(	child, "lens_cal_file",	conf[2].lens_cal_file,	MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_extra_intrinsics.yml");
	json_fetch_int_with_default(	child, "skip_n_frames",	&conf[2].skip_n_frames,	5);

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);

	// make sure at least one detector is enabled
	int n_enabled = 0;
	for(int i=0; i<MAX_CH; i++){
		if(conf[i].enable){
			n_enabled++;
		}
	}
	if(n_enabled<=0){
		fprintf(stderr, "ERROR at least one detector must be enabled\n");
		return -1;
	}

	return 0;
}



#endif // CONFIG_FILE_H