/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <math.h>

#include <modal_json.h>
#include "locations.h"



#define CONFIG_FILE_HEADER "\
/**\n\
 * Apriltag Location Configuration File\n\
 * This file is used by voxl-tag-detector\n\
 *\n\
 * A tag can be flagged by the user as fixed, static, or dynamic.\n\
 *\n\
 *  - fixed: The tag is at a known location in space as described by the\n\
 *    T_tag_wrt_fixed vector and R_tag_to_fixed rotation matrix. These fixed\n\
 *    tags are used by voxl-vision-px4 for apriltag relocalization.\n\
 *\n\
 *  - static: A static tag can be trusted to be static (not moving) but does not\n\
 *    have a known location. For example, a landing pad.\n\
 *\n\
 *  - dynamic: A dynamic tag can be expected to be in motion such as an object\n\
 *    to be tracked or followed dynamically.\n\
 *\n\
 *\n\
 * If the tag is fixed then you must specify its location and rotation relative\n\
 * to the fixed reference frame. Otherwise the translation and rotation\n\
 * parameters can be ignored.\n\
 *\n\
 * The name parameter is currently only used for info prints but can be helpful\n\
 * for the user to keep track of what tag is which.\n\
 * \n\
 * Each tag must have its size listed. Tags not listed will be assumed to have\n\
 * size equal to the default_size.\n\
 *\n\
 * Currently only 36h11 apriltags are supported\n\
 */\n"



void tag_location_print(tag_location_t* t, int n, float default_size_m)
{
	int i,j;
	const char* loc_strings[] = TAG_LOCATION_TYPE_STRINGS;

	printf("default_size_m:      %7.3f\n", (double)default_size_m);
	for(i=0; i<n; i++){
		printf("#%d:\n",i);
		printf("    id:              %d\n", t[i].id);
		printf("    name:            %s\n", t[i].name);
		printf("    loc_type:        %s\n", loc_strings[t[i].loc_type]);
		printf("    size_m:          %7.3f\n", (double)t[i].size_m);
		printf("    T_tag_wrt_fixed:");
		for(j=0;j<3;j++) printf("%4.1f ",t[i].T_tag_wrt_fixed[j]);
		printf("\n    R_tag_to_fixed: ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[0][j]);
		printf("\n                    ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[1][j]);
		printf("\n                    ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[2][j]);
		printf("\n");
	}
	return;
}


int tag_location_read_file(const char* path, tag_location_t* t, int* n, int max_t, float* default_size_m)
{
	// vars and defaults
	int i, m;
	double default_T[3]    = { 0,  0,  0};
	double default_R[3][3] ={{ 0, -1,  0}, \
							 { 1,  0,  0}, \
							 { 0,  0,  1}};
	const char* type_strings[] = TAG_LOCATION_TYPE_STRINGS;

	// set number of locations to 0 at first in case there is an error
	*n = 0;

	//sanity checks
	if(t==NULL || n==NULL || default_size_m==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}
	if(max_t<1){
		fprintf(stderr, "ERROR in %s, maximum number of apriltags must be >=1\n", __FUNCTION__);
		return -1;
	}

	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(path, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new empty json file: %s\n", path);

	// read the data in
	cJSON* parent = json_read_file(path);
	if(parent==NULL) return -1;

	// file structure is just one big array of apriltag location structures
	cJSON* apriltag_json = json_fetch_array_and_add_if_missing(parent, "locations", &m);
	if(m > max_t){
		fprintf(stderr, "ERROR found %d apriltags in file but maximum number of tags is set to %d\n", m, max_t);
		return -1;
	}

	// Empty file found, so add a new empty object to the array.
	if(m == 0){
		cJSON_AddItemToArray(apriltag_json, cJSON_CreateObject());
		m = 1;
		json_set_modified_flag(1); // log that we modified the parent
	}

	// for now, default size is the only thing not in the array
	json_fetch_float_with_default(parent, "default_size_m", default_size_m, 0.4);

	// copy out each item in the array
	for(i=0; i<m; i++){
		cJSON* item = cJSON_GetArrayItem(apriltag_json, i);
		json_fetch_int_with_default(		item, "id",				&t[i].id, i);
		json_fetch_string_with_default(		item, "name",			t[i].name, TAG_NAME_LEN, "default_name");
		json_fetch_enum_with_default(		item, "loc_type",		(int*)&t[i].loc_type, type_strings,	N_TAG_LOCATION_TYPES, 0);
		json_fetch_float_with_default(		item, "size_m",			&t[i].size_m, 0.4);
		json_fetch_fixed_vector_with_default(item,"T_tag_wrt_fixed",t[i].T_tag_wrt_fixed, 3, default_T);
		json_fetch_fixed_matrix_with_default(item,"R_tag_to_fixed",	&t[i].R_tag_to_fixed[0][0], 3, 3, &default_R[0][0]);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", path);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON tag location data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(path, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	*n = m;
	return 0;
}

